/**
 * @author            : Amit Singh
 * @description       :
 * @lastModifiedOn    : 29-08-2021
 * @lastModifiedBy    : Lim Kian Tat (Tedy)
 * Modifications Log 
 * Ver   Date         Author       Modification
 * 1.0   29-08-2021   Tedy         Initial Version
**/

import { LightningElement, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
// import { getRecordNotifyChange } from 'lightning/uiRecordApi';
import { updateRecord } from 'lightning/uiRecordApi';
import selectContact from '@salesforce/apex/LWCContactLookupController.selectContact';

export default class CustomLookup extends LightningElement {

    @api recordId;

    fields = ["Name", "Email", "Phone"];
    displayFields = 'Name, Email, Phone'

    async handleLookup(event) {
        let contactId = event.detail.data.record.Id;
        let accountId = this.recordId;
        let contactName = event.detail.data.record.Name;
        await selectContact({ accountId, contactId }).then(result => {
            if (result === true) {

                this.showToast(`Contact '${contactName}' have linked to this account.`, "View {0}.", [
                    {
                        url: '/' + event.detail.data.record.Id,
                        label: 'contact record'
                    }
                ], "success");
                this.template.querySelector('c-search-component').reset();
            }
        }).catch(error => {
            this.showToast(`Error Found`, JSON.stringify(error), [], "error");
        }).finally(() => {
        });
        // Notify LDS that you've changed the record outside its mechanisms.
        eval("$A.get('e.force:refreshView').fire();");
        // getRecordNotifyChange([{ recordId: accountId }]); sadly tested, doesn't work with related list.

    }

    showToast(title = "", message = "", messageData = [], variant = "success") {
        const event = new ShowToastEvent({
            title,
            message,
            messageData,
            variant
        });
        this.dispatchEvent(event);
    }
}