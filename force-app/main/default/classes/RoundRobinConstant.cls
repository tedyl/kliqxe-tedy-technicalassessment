public class RoundRobinConstant {

    public static final String AssignmentRuleRegex 			= '(?<key>[^=]+)=(?<value>[^\\|]+)\\|?'; // E.g. BillingCountry=Singapore|BillingCity=Hello
    public static final String ValueAssignmentSymbol 		= '=';
    public static final String ValueSplitterSymbol 			= ',';
    public static final String VariableSplittersSymbol		= '|';
    
}