/*
* @author            : Lim Kian Tat (Tedy)
* @description       : LWC Search Controller with respect user record sharing.
* @lastModifiedOn    : 29-08-2021
* @lastModifiedBy    : Lim Kian Tat (Tedy)
* Modifications Log 
* Ver   Date         Author       Modification
* 1.0   29-08-2021   Tedy         Initial Version
*/

public with sharing class LWCSearchController {

   @AuraEnabled
    public static List<sObject> search(String objectName, List<String> fields, String searchTerm){
        String searchKeyword = searchTerm + '*';
        String returningQuery = '';
        returningQuery = objectName+' ( Id, '+String.join(fields,',')+')';
        String query = 'FIND :searchKeyword IN ALL FIELDS RETURNING '+returningQuery+' LIMIT 20';
        List<List<sObject>> searchRecords = Search.Query(Query);
        return searchRecords.get(0);
    }
}