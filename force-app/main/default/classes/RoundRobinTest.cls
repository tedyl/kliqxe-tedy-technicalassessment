@IsTest
public class RoundRobinTest {
	
    @testSetup static void setup() {
        
        // Setup Users Test Records
        List<User> ListUser = new List<User>();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        ListUser.add(new User(Alias = 'sa1', Email='sa1@example.com', LastName='Sales Agent 1', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='sa-1kmalaysia@example.test'));
        ListUser.add(new User(Alias = 'sa2', Email='sa2@example.com', LastName='Sales Agent 2', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='sa-2kmalaysia@example.test'));
        ListUser.add(new User(Alias = 'sa3', Email='sa3@example.com', LastName='Sales Agent 3', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='sa-3kmalaysia@example.test'));
        ListUser.add(new User(Alias = 'sa4', Email='sa4@example.com', LastName='Sales Agent 4', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='sa-4kmalaysia@example.test'));
        insert ListUser;
        System.debug(ListUser);
        // Setup Round Robin Test Records
        List<Round_Robin__c> ListRoundRobin = new List<Round_Robin__c>();
       	ListRoundRobin.add(new Round_Robin__c(Name = 'Malaysia Account Round Robin', API_Name__c = 'RR_ACC_MY', Is_Active__c = TRUE, Queue_Order__c = 1, Matching_Rule__c = 'BillingCountry=Malaysia', sObject__c = 'Account'));
        ListRoundRobin.add(new Round_Robin__c(Name = 'Singapore Account Round Robin', API_Name__c = 'RR_ACC_SG', Is_Active__c = TRUE, Queue_Order__c = 1, Matching_Rule__c = 'BillingCountry=Singapore', sObject__c = 'Account'));
        insert ListRoundRobin;   
         // Setup Round Robin Assignee Test Records
        List<Round_Robin_Assignee__c> ListRoundRobinAssignee = new List<Round_Robin_Assignee__c>();
        ListRoundRobinAssignee.add(new Round_Robin_Assignee__c(User__c = ListUser.get(0).Id, Is_Active__c = TRUE, Queue_Order__c = 1, Round_Robin__c = ListRoundRobin.get(0).Id));
        ListRoundRobinAssignee.add(new Round_Robin_Assignee__c(User__c = ListUser.get(1).Id, Is_Active__c = TRUE, Queue_Order__c = 2, Round_Robin__c = ListRoundRobin.get(0).Id));
        ListRoundRobinAssignee.add(new Round_Robin_Assignee__c(User__c = ListUser.get(2).Id, Is_Active__c = TRUE, Queue_Order__c = 1, Round_Robin__c = ListRoundRobin.get(1).Id));
        ListRoundRobinAssignee.add(new Round_Robin_Assignee__c(User__c = ListUser.get(3).Id, Is_Active__c = TRUE, Queue_Order__c = 2, Round_Robin__c = ListRoundRobin.get(1).Id));
        insert ListRoundRobinAssignee;  
    }
    
    @isTest static void TestScenarios() {
 		Map<Id, User> User = new Map<Id, User>([SELECT Id, LastName FROM User WHERE LastName IN ('Sales Agent 1', 'Sales Agent 2', 'Sales Agent 3', 'Sales Agent 4')]);
        
        Test.startTest();
        insert new Account(Name='Account 1', BillingCountry = 'Malaysia');
        insert new Account(Name='Account 2', BillingCountry = 'Singapore');
        insert new Account(Name='Account 3', BillingCountry = 'Singapore');
        insert new Account(Name='Account 4', BillingCountry = 'Malaysia');
        insert new Account(Name='Account 5', BillingCountry = 'Malaysia');
        
        Account Account1 = [SELECT OwnerId FROM Account WHERE Name='Account 1' LIMIT 1];
        System.assertEquals('Sales Agent 1', User.get(Account1.OwnerId).LastName);
        
        Account Account2 = [SELECT OwnerId FROM Account WHERE Name='Account 2' LIMIT 1];
        System.assertEquals('Sales Agent 3', User.get(Account2.OwnerId).LastName);
        
        Account Account3 = [SELECT OwnerId FROM Account WHERE Name='Account 3' LIMIT 1];
        System.assertEquals('Sales Agent 4', User.get(Account3.OwnerId).LastName);
        
        Account Account4 = [SELECT OwnerId FROM Account WHERE Name='Account 4' LIMIT 1];
        System.assertEquals('Sales Agent 2', User.get(Account4.OwnerId).LastName);
        
        Account Account5 = [SELECT OwnerId FROM Account WHERE Name='Account 5' LIMIT 1];
        System.assertEquals('Sales Agent 1', User.get(Account5.OwnerId).LastName);
       	
        List<Account> ListAccount = new List<Account>();
        ListAccount.add(new Account(Name='Account 6', BillingCountry = 'Singapore'));
        ListAccount.add(new Account(Name='Account 7', BillingCountry = 'Malaysia'));
        insert ListAccount;
        
        Account Account6 = [SELECT OwnerId FROM Account WHERE Name='Account 6' LIMIT 1];
        System.assertEquals('Sales Agent 3', User.get(Account6.OwnerId).LastName);
        
        Account Account7 = [SELECT OwnerId FROM Account WHERE Name='Account 7' LIMIT 1];
        System.assertEquals('Sales Agent 2', User.get(Account7.OwnerId).LastName);
        
      	// Set Sales Agent 4 to Malaysia Team
      	Round_Robin__c TargetedRoundRobin = [SELECT Id FROM Round_Robin__c WHERE API_Name__c = 'RR_ACC_MY' LIMIT 1];
        User SalesAgent4 = [SELECT Id, LastName FROM User WHERE UserName='sa-4kmalaysia@example.test' LIMIT 1];
        insert (new Round_Robin_Assignee__c(User__c = SalesAgent4.Id, Is_Active__c = TRUE, Queue_Order__c = 3, Round_Robin__c = TargetedRoundRobin.Id));
        insert new Account(Name='Account 8', BillingCountry = 'Malaysia');
        Account Account8 = [SELECT OwnerId FROM Account WHERE Name='Account 8' LIMIT 1];
        System.assertEquals('Sales Agent 4', SalesAgent4.LastName);
        
        Test.stopTest();
        // Perform some testing
    }
}