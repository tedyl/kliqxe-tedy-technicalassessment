/*
* @author            : Lim Kian Tat (Tedy)
* @description       : Round Robin Engine
* @lastModifiedOn    : 29-08-2021
* @lastModifiedBy    : Lim Kian Tat (Tedy)
* Modifications Log 
* Ver   Date         Author       Modification
* 1.0   28-08-2021   Tedy         Initial Version
* 1.1   29-08-2021   Tedy         Support Multiple sObjects.
* Backlog
* @TODO: Enable dynamic field mapping logic in matching rules. For now, it's match by BillingCountry in Account, Country in Lead (extra test).
* @TODO: Enable Conditional Matching logic. And & Or Filter.
*/

public class RoundRobin {
    
    private static Map<Id, Round_Robin__c> MapRoundRobins = new Map<Id, Round_Robin__c>([SELECT Id, Name, Matching_Rule__c, Current_Round_Robin_Assignee__c, Total_Active_Assignee__c, sObject__c, (SELECT Id, Name, User__c, Queue_Order__c, Round_Robin__c FROM Round_Robin_Assignees__r WHERE Is_Active__c = TRUE AND User__c != NULL ORDER BY Queue_Order__c, User__c ASC) FROM Round_Robin__c WHERE Is_Active__c = TRUE ORDER BY Queue_Order__c, Name ASC]);
    private static Map<Id, Round_Robin_Assignee__c> MapRoundRobinsAssignee = new Map<Id, Round_Robin_Assignee__c>([SELECT Id, Name, User__c, Queue_Order__c, Round_Robin__c FROM Round_Robin_Assignee__c WHERE Is_Active__c = TRUE AND User__c != NULL ORDER BY Queue_Order__c, User__c ASC]);
    // create a matching rule. Target sObject as Root Key. Round Robin Id as Child Key. Field API Name as Grandchild Key. Expected field value as Grandchild Value (Supported array)
    private static Map<String, Map<Id, Map<String, List<String>>>> MapRRIdMatchingRulesWithValues = new Map<String, Map<Id, Map<String, List<String>>>>();
    
    private static List <SObject> ListSObject;
    private static List <Round_Robin__c> ListPendingUpdateCurrentAssigneeRoundRobin;
    
    // Creating reusable sObject round robin assignment rule
    @InvocableMethod(label='Round Robin Assignment' description='Assign owner based on round robin assignments')
    public static void execute (List<Requests> RequestList) {
		
        // setup round robin rules 
        SetupRoundRobinMatchingRules();

        // if matching rules setup is not empty.
        if (MapRRIdMatchingRulesWithValues.isEmpty() == false){
            for(Requests request: requestList){
                String sObjectName = request.InputCollection.Id.getSObjectType().getDescribe().getName();
                if (sObjectName == 'Account') ListSObject.add(assignmentForAccounts(request.inputCollection));
                if (sObjectName == 'Lead') ListSObject.add(assignmentForLeads(request.inputCollection));
            }
        }
        
       	// update any listobject in bulk
        if (ListSObject.size() > 0){
            update ListSObject;
        }
        
        // update any round robin in bulk
        if (ListPendingUpdateCurrentAssigneeRoundRobin.size() > 0){
            update ListPendingUpdateCurrentAssigneeRoundRobin;
        }
      
    }
    
    private static SObject assignmentForAccounts(SObject account){
		
        // @TODO: Map all fields with sObject and it value with matching rule. 
        // For now: Find BillingCountry will do.
        List <Id> RoundRobinMatchedIds = GetRoundRobinIdsByKeyPairs('Account', 'BillingCountry', String.valueOf(account.get('BillingCountry')));
        List<Round_Robin__c> ListRoundRobin = new List<Round_Robin__c>();
        for(Id RoundRobinMatchedId : RoundRobinMatchedIds){
            ListRoundRobin.add(MapRoundRobins.get(RoundRobinMatchedId));
        }
        
        if (RoundRobinMatchedIds.IsEmpty() == false) account.put('OwnerId', GetNextRoundRobinAssignee(ListRoundRobin)?.User__c);
        account.put('Set_Owner_by_RR__c', false);
       
        return account;
        
    }
    
    private static SObject assignmentForLeads(SObject lead){
		
        // @TODO: Map all fields with sObject and it value with matching rule. 
        // For now: Find Country will do.
        List <Id> RoundRobinMatchedIds = GetRoundRobinIdsByKeyPairs('Lead', 'Country', String.valueOf(lead.get('Country')));
        List<Round_Robin__c> ListRoundRobin = new List<Round_Robin__c>();
        for(Id RoundRobinMatchedId : RoundRobinMatchedIds){
            ListRoundRobin.add(MapRoundRobins.get(RoundRobinMatchedId));
        }
        
        if (RoundRobinMatchedIds.IsEmpty() == false) lead.put('OwnerId', GetNextRoundRobinAssignee(ListRoundRobin)?.User__c);
        lead.put('Set_Owner_by_RR__c', false);
       
        return lead;
        
    }

    private static Round_Robin_Assignee__c GetNextRoundRobinAssignee(List<Round_Robin__c> ListRoundRobin){
        Round_Robin_Assignee__c RoundRobinAssignee;

       	// if Active Assignee of the RoundRobin is empty, next RoundRobin, otherwise return empty.
       	// if Active Assignee of the RoundRobin is found, and only 1 active, return same as current. 
       	// if Active Assignee of the RoundRobin is more than 1, then find next assignee.
        for(Round_Robin__c RoundRobin : ListRoundRobin){
            Integer RoundRobinAssigneesCount = RoundRobin.Round_Robin_Assignees__r.size();
            if (RoundRobinAssigneesCount == 0 || RoundRobinAssignee != null){
                continue;
            } else if (RoundRobinAssigneesCount == 1){
                RoundRobinAssignee = RoundRobin.Round_Robin_Assignees__r[0];
            } else {                
                Integer CurrentAssigneeIndex = RoundRobin.Round_Robin_Assignees__r.indexOf(MapRoundRobinsAssignee.get(RoundRobin.Current_Round_Robin_Assignee__c));
                Integer MaximumCount = (Integer) RoundRobin.Total_Active_Assignee__c;
                // if Current_Round_Robin_Assignee__c is empty or last, set the first assignee as current Current_Round_Robin_Assignee__c value
                if (CurrentAssigneeIndex == -1 || MaximumCount == (CurrentAssigneeIndex + 1)){
                    RoundRobinAssignee = RoundRobin.Round_Robin_Assignees__r[0];
                    
                } else {
                    RoundRobinAssignee = RoundRobin.Round_Robin_Assignees__r.get(CurrentAssigneeIndex + 1);
                }
            }
            RoundRobin.Current_Round_Robin_Assignee__c = RoundRobinAssignee.Id;
            ListPendingUpdateCurrentAssigneeRoundRobin.add(RoundRobin);
        }
        
        //System.debug('@@@RoundRobinAssignee = ' + RoundRobinAssignee);  
       
        return RoundRobinAssignee;
    }
    
    
    // get the round robin id by target the column name (TargetKey) and the value (TargetValue) to find the matching round robin id and return. 
    // potentially you can match more than one round robin record if the criteria match.
    private static List<Id> GetRoundRobinIdsByKeyPairs(String TargetObject, String TargetKey, String TargetValue){
        List<Id> RoundRobinIds = new List<Id>();
        // get matching rules.
        Map<Id, Map<String, List<String>>> MapObjectMatchingRules = MapRRIdMatchingRulesWithValues.get(TargetObject);
        if (MapObjectMatchingRules != NULL){
            for(Id RoundRobinId : MapObjectMatchingRules.keyset()){
                Map<String, List<String>> KeyPairs = MapObjectMatchingRules.get(RoundRobinId);
                List<String> PairValues = KeyPairs.get(TargetKey);
                if (PairValues.isEmpty() == false && PairValues.contains(TargetValue)){
                    RoundRobinIds.add(RoundRobinId);
                }
            }
        }
        return RoundRobinIds;
    }
    
    // Group all the expected matching field API Name as Key, and expected value as Value.
    private static void SetupRoundRobinMatchingRules() {
        
        ListSObject = new List <SObject>();
        ListPendingUpdateCurrentAssigneeRoundRobin = new List <Round_Robin__c>();
        
        for(Round_Robin__c RoundRobin: MapRoundRobins.values()){
            Id RoundRobinId 	= RoundRobin.Id;
            String sObjectName 	= RoundRobin.sObject__c;
            // Setup sObject value as key.
            if (MapRRIdMatchingRulesWithValues.get(sObjectName) == NULL){
            	MapRRIdMatchingRulesWithValues.put(sObjectName, new Map<Id, Map<String, List<String>>>());    
            }
            // In each sobject, there will be more than 1 round robin. Setup RoundRobin Id as Children Key. 
            MapRRIdMatchingRulesWithValues.get(sObjectName).put(RoundRobinId, new Map<String, List<String>>());
            // Get round robin regex constant
            Pattern MyPattern = Pattern.compile(RoundRobinConstant.AssignmentRuleRegex);
            // Setup round robin regex logic
            Matcher MyMatcher = MyPattern.matcher(RoundRobin.Matching_Rule__c);
            
            while (MyMatcher.find()) { 
                String MatchingLogic 	= MyMatcher.group();
                List <String> KeyPairs 	= MatchingLogic.split(RoundRobinConstant.ValueAssignmentSymbol);
                // Assign value to existing key 
                if (MapRRIdMatchingRulesWithValues.get(sObjectName).get(RoundRobinId).get(KeyPairs[0]) == null) MapRRIdMatchingRulesWithValues.get(sObjectName).get(RoundRobinId).put(KeyPairs[0], new List<String>());
                
                // @TODO: Needs to support integer, boolean, number data type.
                if (KeyPairs[1] != null){
                    List <String> Values = KeyPairs[1].split(RoundRobinConstant.ValueSplitterSymbol);    
                    for(String Value : Values){
                        // Trim all the variable splitter symbol before assign value to key
                        MapRRIdMatchingRulesWithValues.get(sObjectName).get(RoundRobinId).get(KeyPairs[0]).add(KeyPairs[1].substringBefore(RoundRobinConstant.VariableSplittersSymbol));
                    }
                } else {
                    MapRRIdMatchingRulesWithValues.get(sObjectName).get(RoundRobinId).get(KeyPairs[0]).add(null);
                }
                
            }
        }
	}
    
    public class Requests {
        @InvocableVariable(label='Records for Input' description='Please ensure the object has owner' required=true)
        public SObject InputCollection;
    }
    
    
}