/*
* @author            : Lim Kian Tat (Tedy)
* @description       : LWC Search Controller with respect user record sharing.
* @lastModifiedOn    : 29-08-2021
* @lastModifiedBy    : Lim Kian Tat (Tedy)
* Modifications Log 
* Ver   Date         Author       Modification
* 1.0   29-08-2021   Tedy         Initial Version
*/

public with sharing class LWCContactLookupController {

    @AuraEnabled
    public static Boolean selectContact(Id accountId, Id contactId){
        Contact contact = [SELECT Id, AccountId FROM Contact WHERE ID =: contactId LIMIT 1];
        contact.AccountId = accountId;
        update contact;
        return true;
    }
}